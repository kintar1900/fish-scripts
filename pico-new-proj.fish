#!/usr/bin/env fish

if ! set -q PICO_SDK_PATH
    echo "PIC_SDK_PATH is not set"
    exit 1
end

if ! set -q argv[1]
    set scriptName = (status --current-filename)
    echo ""
    echo "-----------------------------------------------------------------"
    echo "USAGE: $scriptName <project name> [--<library> [--<library>]... ]"
    echo "  Where <library> is one of: i2c spi pio multicore"
    echo "-----------------------------------------------------------------"
    echo ""
    exit 254
end

set projName $argv[1]
set argc (count $argv[2..-1])
set linkLibs pico_stdlib
set newline \n
set includeLines '#include <stdio.h>
#include "pico/stdlib.h"'

set enableLines ''

for opt in (seq 2 (count $argv))
    switch $argv[$opt]
    case "--i2c"
        set linkLibs $linkLibs hardware_i2c
        set includeLines $includeLines \n '#include "hardware/i2c.h"'
    case "--spi"
        set linkLibs $linkLibs hardware_spi
        set includeLines $includeLines \n '#include "hardware/spi.h"'
    case "--pio"
        set linkLibs $linkLibs hardware_pio
        set includeLines $includeLines \n '#include "hardware/pio.h"'
    case "--multicore"
        set linkLibs $linkLibs pico_multicore
        set includeLines $includeLines \n '#include "pico/multicore.h"'
    case "--stdio-usb"
    	set enableLines $enableLines \n 'pico_enable_stdio_usb($projName 1)'
    case "--stdio-uart"
    	set enableLines $enableLines \n 'pico_enable_stdio_uart($projName 1)'
    end
end

if test -e ~/$projName
    echo "Directory $projName already exists"
    exit 1
end

mkdir $projName && cd $projName
git init
cp $PICO_SDK_PATH/external/pico_sdk_import.cmake .

echo >CMakeLists.txt "\
cmake_minimum_required(VERSION 3.13)

# Pull in SDK (must be before project)
include(pico_sdk_import.cmake)

project($projName C CXX ASM)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

if (PICO_SDK_VERSION_STRING VERSION_LESS \"1.5.0\")
    message(FATAL_ERROR \"Raspberry Pi Pico SDK version 1.5.0 (or later) required. Your version is $PICO_SDK_VERSION_STRING\")
endif()

# Initialize the SDK
pico_sdk_init()

add_executable($projName
    main.c
    )

target_link_libraries($projName $linkLibs)
pico_add_extra_outputs($projName)

"
echo >main.c "\
$includeLines

int main() {
    return 0;
}
"

echo >.gitignore '
.vscode
_deps
cmake-*
build
.DS_Store
'

git add .gitignore
git checkout -b main
git commit -m "Initial commit"

